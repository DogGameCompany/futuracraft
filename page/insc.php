<?php

$bdd = new PDO('mysql:host=127.0.0.1;dbname=espace_membre', 'root', '');

if(isset($_POST['forminscription']))
{
	$pseudo = htmlspecialchars($_POST['pseudo']);
	$mail = htmlspecialchars($_POST['mail']);
	$mail2 = htmlspecialchars($_POST['mail2']);
	$mdp = sha1($_POST['mdp']);
	$mdp2 = sha1($_POST['mdp2']);

	if(!empty($_POST['pseudo']) AND !empty($_POST['mail']) AND !empty($_POST['mail2']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2']))
	{
		
		$pseudolenght = strlen($pseudo);
		if($pseudolenght <= 15)
		{
			if($mail == $mail2)
			{
				if(filter_var($mail, FILTER_VALIDATE_EMAIL))
				{
					$reqmail = $bdd->prepare("SELECT * FROM membres WHERE mail = ?");
					$reqmail->execute(array($mail));
					$mailexist = $reqmail->rowCount();
					if($mailexist == 0)
					{
						$reqpseudo = $bdd->prepare("SELECT * FROM membres WHERE pseudo = ?");
						$reqpseudo->execute(array($pseudo));
						$pseudoexist = $reqpseudo->rowCount();
						if($pseudoexist == 0)
						{
							if($mdp == $mdp2)
							{
								$insertmbr = $bdd->prepare("INSERT INTO membres(pseudo, mail, motdepasse) VALUES(?, ?, ?)");
								$insertmbr->execute(array($pseudo, $mail, $mdp));
								header('Location: ../index.html');
								exit();
							}
							else
							{
								$erreur = "Vos mots de passe ne correspondent pas !";
							}
						}
						else
						{
							$erreur = "Pseudo déja utilisée !";
						}
					}
					else
					{
						$erreur = "Adresse mail déja utilisée !";
					}
				}
				else
				{
					$erreur = "Votre adresse mail n'est pas valide !";
				}
			}
			else
			{
				$erreur = "Vos adresses mail ne correspondent pas !";
			}
		}
		else
		{
			$erreur = "Votre pseudo ne doit pas dépasser 15 caractères !";
		}
	}
	else
	{
		$erreur = "Tous les champs doivent être complétés !";
	}
}

?>
<!DOCTYPE html>
<html lang="fr-fr">
<head>
	<meta charset="utf-8">
	<title>Inscription | FuturaCraft</title>
	<link rel="stylesheet" href="../css/styleSignin.css"/>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfxpqpzzvqgk6tah5pvlgofqnhsod2xbe+qkpxcaflneevoeh3sl0sibvcoqvnn" crossorigin="anonymous">


	<link rel="shortcut icon" href="../img/FC_logo4.png">
</head>
<body>
	<div class="content-titre">
		<ul class="menu-nav">
			<li>
				<a class="lien-item" href="../index.html">Accueil</a>
			</li>
			<li>
				<a class="lien-item" href="download.html">Téléchargement</a>
			</li>
			<li>
				<a class="lien-item" href="news.html">News</a>
			</li>
			<li>
				<a class="lien-item" href="contact.html">Contact</a>
			</li>
		</ul>

		<div class="lien-content-conect-insc">
			<div>
				<a class="insc" href="#">Inscription</a>
				<a class="conect" href="conect.html">Connexion</a>
			</div>

			<h1 class="titre">FuturaCraft</h1>
		</div>
	</div>
	<div class="content-page">

		<div id="container">

			<?php

			if (isset($erreur))
			{
				echo '<center><font color="red">'.$erreur.'</font></center>';
			}

			?>

			<br>

			<!-- zone de connexion -->

			<form method="POST" action="">
				<h1>Inscription</h1>

				<label for="pseudo"><b>Pseudo</b></label>
				<input type="text" placeholder="Entrer le pseudo" id="pseudo" name="pseudo" value="<?php if(isset($pseudo)) { echo $pseudo; } ?>">

				<label for="mail"><b>Adresse email</b></label>
				<input type="email" placeholder="Entrer l'adresse email" id="mail" name="mail" value="<?php if(isset($mail)) { echo $mail; } ?>">

				<label for="mail2"><b>Confirmation : Email</b></label>
				<input type="email" placeholder="Confirmer l'adresse email" id="mail2" name="mail2" value="<?php if(isset($mail2)) { echo $mail2; } ?>">

				<label for="mdp"><b>Mot de passe</b></label>
				<input type="password" placeholder="Entrer le mot de passe" id="mdp" name="mdp">

				<label for="mdp2"><b>Confirmation : Mot de passe</b></label>
				<input type="password" placeholder="Confirmer le mot de passe" id="mdp2" name="mdp2">

				<br><br>

				<input type="submit" id='submit' name="forminscription" value="S'INSCRIRE" >
			</form>
		</div>

	</div>
</body>
</html>